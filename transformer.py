#Step 1: Import necessary libraries
import sys
import pandas as pd
import time
from rdflib import Graph, Namespace, URIRef, Literal
from rdflib.namespace import RDF, XSD

#Step 2: Replace NaN values in the dataset with empty strings
def preprocess_data(file_path):
    try:
        dataset = pd.read_csv(file_path, nrows=50)  # Load only the first 50 rows to avoid memory issues with large files
        return dataset.fillna("")
    except FileNotFoundError:
        print(f"Error: The file '{file_path}' does not exist.")
        sys.exit(1)
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
        sys.exit(1)

#Step 3: Create RDF graph using SAREF ontology
def initialize_graph():
    g = Graph()
    saref = Namespace("https://saref.etsi.org/core/")
    ex = Namespace("https://data.open-power-system-data.org/household_data/2020-04-15#")
    saref4bldg = Namespace("https://saref4bldg.example.com/")
    g.bind("saref", saref)
    g.bind("ex", ex)
    g.bind("saref4bldg", saref4bldg)
    return g, saref, ex, saref4bldg

#Step 4: Generate RDF Triples from the dataset
def generate_rdf(graph, dataset, saref, ex, saref4bldg):
    for index, row in dataset.iterrows():
        timestamp = Literal(row['utc_timestamp'], datatype=XSD.dateTime)
        for col in dataset.columns:
            if col not in ['utc_timestamp', 'cet_cest_timestamp', 'interpolated']:
                building_type = Literal(col.split('_')[2], datatype=XSD.string)
                device = Literal(col, datatype=XSD.string)
                value = row[col]
                if value:
                    measurement_uri = URIRef(ex[f"measurement/{col}/{index}"])
                    graph.add((measurement_uri, RDF.type, saref.Measurement))
                    graph.add((measurement_uri, saref.hasTimestamp, timestamp))
                    graph.add((measurement_uri, saref.hasValue, Literal(value, datatype=XSD.float)))
                    graph.add((measurement_uri, saref.UnitOfMeasure, Literal("kWh")))
                    graph.add((measurement_uri, saref4bldg.Location, building_type))
                    graph.add((measurement_uri, saref.Device, device))

#Step 5: Serialize the RDF graph to Turtle format
def serialize_graph(graph, file_name):
    turtle_data = graph.serialize(format="turtle")
    with open(file_name, "w") as f:
        f.write(turtle_data)

#Step 6: Main processing function
def main():
    if len(sys.argv) < 2:
        print("Usage: python transformer.py <path/to/dataset.csv>")
        sys.exit(1)
    file_path = sys.argv[1]
    start_time = time.time()
    datafile = preprocess_data(file_path)
    graph, saref, ex, saref4bldg = initialize_graph()
    generate_rdf(graph, datafile, saref, ex, saref4bldg)
    serialize_graph(graph, "graph.ttl")
    print(f"The RDF graph serialized to graph.ttl successfully in {time.time() - start_time} seconds")

if __name__ == "__main__":
    main()
